#include "HelperFunctions.h"

namespace HelperFunctions {
	void swap(int* a, int* b) {
		int tmp = *a;
		*a = *b;
		*b = tmp;
	}

	int mod(int a) {
		if (a > 0) {
			return a;
		}
		else if (a < 0) {
			return -a;
		}
		return 0;
	}

	double mod(double a) {
		if (a > 0) {
			return a;
		}
		else if (a < 0) {
			return -a;
		}
		return 0;
	}

	Vector3f rgbToHsv(Vector3f RGB) {
		float H, S, V;
		
		float R = RGB.dx / 255;
		float G = RGB.dy / 255;
		float B = RGB.dz / 255;
		float MAX = R;
		float MIN = R;
		if (G > MAX) {
			MAX = G;
		}
		if (B > MAX) {
			MAX = B;
		}
		if (G < MIN) {
			MIN = G;
		}
		if (B < MIN) {
			MIN = B;
		}
	
		// calc H
		if (MAX == MIN) {
			H = 0;
		} else if (MAX == R && G >= B) {
			H = 60 * (G - B) / (MAX - MIN) + 0;
		} else if (MAX == R && G < B) {
			H = 60 * (G - B) / (MAX - MIN) + 360;
		} else if (MAX == G) {
			H = 60 * (B - R) / (MAX - MIN) + 120;
		} else if (MAX == B) {
			H = 60 * (R - G) / (MAX - MIN) + 240;
		}

		// calc S
		if (MAX == 0) {
			S = 0;
		} else {
			S = 1 - (MIN / MAX);
		}

		// calc V
		V = MAX;

		return Vector3f(H, S, V);
	}

	Vector3f hsvToRgb(Vector3f HSV) {
		float H = HSV.dx;
		float S = HSV.dy;
		float V = HSV.dz;

		float C = V * S;

		float X = C * (1 -
			abs(((int)(H / 60)) % 2 - 1)
		);

		float R, G, B;
		if (H >= 0 && H < 60) {
			R = C;
			G = X;
			B = 0;
		} else if (H >= 60 && H < 120) {
			R = X;
			G = C;
			B = 0;
		} else if (H >= 120 && H < 180) {
			R = 0;
			G = C;
			B = X;
		} else if (H >= 180 && H < 240) {
			R = 0;
			G = X;
			B = C;
		} else if (H >= 240 && H < 300) {
			R = X;
			G = 0;
			B = C;
		} else if (H >= 300 && H < 360) {
			R = C;
			G = 0;
			B = X;
		}

		float m = V - C;
		R = 255 * (R + m);
		G = 255 * (G + m);
		B = 255 * (B + m);

		if (R > 255) {
			R = 255;
		}
		if (G > 255) {
			G = 255;
		}
		if (B > 255) {
			B = 255;
		}

		return Vector3f(R, G, B);
	}

	COLORREF rgbToColorref(Vector3f RGB) {
		return RGB.dx + 256 * (int) RGB.dy + 256 * 256 * (int) RGB.dz;
	}

	Vector3f colorrefToRgb(COLORREF color) {
		Vector3f rgb(0, 0, 0);

		rgb.dx = color % 256;
		rgb.dy = (color / 256) % 256;
		rgb.dz = (color / (256*256)) % 256;
		return rgb;
	}
}

