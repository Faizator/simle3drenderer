#pragma once

#include <iostream>
#include <Windows.h>

namespace Log {
	void debug(const char* msg);
	void debug(const char* msg, int num);
	void debug(const char* msg, double num);
}