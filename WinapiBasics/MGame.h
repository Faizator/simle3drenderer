#pragma once

#include "DrawingFunctions.h"

class MGame {
private:
	int xPos = 0;
	int grad = 0;
	void innerPaint(HDC hdc, RECT& rc);
public:
	bool isRunning = false;
	bool paused = false;
	bool lButtonPressed = false;
	void onPaint(HWND hWnd, LPPAINTSTRUCT lpPS);
	bool update();
};

class HdcPixelSetter : public DrawingFunctions::IPixelSetter {
	private: 
		HDC hdc;

	public:
		HdcPixelSetter(HDC hdc) {
			this->hdc = hdc;
		}

		void setPixel(int x, int y, COLORREF color) {
			SetPixel(this->hdc, x, y, color);
		}
};