#include "MathFunctions.h"
#include <math.h>

double MathFunctions::gradToRad(int grad) {
	return grad * PI / 180;
}

int MathFunctions::pseudoScalar(Vector2 a, Vector2 b) {
	return a.dx * b.dy - a.dy * b.dx;
}

BoundingRect2D MathFunctions::boundingRectFromTriangle(Triangle2D triangle) {
	int minX = minOf(triangle.x0, triangle.x1, triangle.x2);
	int maxX = maxOf(triangle.x0, triangle.x1, triangle.x2);

	int minY = minOf(triangle.y0, triangle.y1, triangle.y2);
	int maxY = maxOf(triangle.y0, triangle.y1, triangle.y2);

	return BoundingRect2D(minX, maxX, minY, maxY);
}


BoundingRect3D MathFunctions::boundingRectFromTriangle(Triangle3D triangle) {
	int minX = minOf(triangle.x0, triangle.x1, triangle.x2);
	int maxX = maxOf(triangle.x0, triangle.x1, triangle.x2);

	int minY = minOf(triangle.y0, triangle.y1, triangle.y2);
	int maxY = maxOf(triangle.y0, triangle.y1, triangle.y2);

	int leftZ = triangle.z0;
	int rightZ = triangle.z0;

	if (minX == triangle.x1) {
		leftZ = triangle.z1;
	} else if (minX == triangle.x2) {
		leftZ = triangle.z2;
	}

	if (maxX == triangle.x1) {
		rightZ = triangle.z1;
	} else if (maxX == triangle.x2) {
		rightZ = triangle.z2;
	}

	return BoundingRect3D(minX, maxX, minY, maxY, leftZ, rightZ);
}

int MathFunctions::minOf(int a, int b) {
	if (a < b) {
		return a;
	}
	return b;
}

int MathFunctions::minOf(int a, int b, int c) {
	int minAB = minOf(a, b);
	return minOf(minAB, c);
}

int MathFunctions::maxOf(int a, int b) {
	if (a > b) {
		return a;
	}
	return b;
}

int MathFunctions::maxOf(int a, int b, int c) {
	int maxAB = maxOf(a, b);
	return maxOf(maxAB, c);
}

bool MathFunctions::pointInTriangle(Triangle2D triangle, Point2 point) {

	Vector2 vecA = Vector2(triangle.x1 - triangle.x0, triangle.y1 - triangle.y0);
	Vector2 vecB = Vector2(    point.x - triangle.x0,     point.y - triangle.y0);
	
	if (pseudoScalar(vecA, vecB) < 0) {
		return false;
	}

	vecA = Vector2(triangle.x2 - triangle.x1, triangle.y2 - triangle.y1);
	vecB = Vector2(    point.x - triangle.x1,     point.y - triangle.y1);

	if (pseudoScalar(vecA, vecB) < 0) {
		return false;
	}

	vecA = Vector2(triangle.x0 - triangle.x2, triangle.y0 - triangle.y2);
	vecB = Vector2(    point.x - triangle.x2,     point.y - triangle.y2);

	if (pseudoScalar(vecA, vecB) < 0) {
		return false;
	}

	return true;
}

Triangle2D MathFunctions::to2D(Triangle3D triangle3D) {
	return Triangle2D(triangle3D.x0, triangle3D.y0, triangle3D.x1, triangle3D.y1, triangle3D.x2, triangle3D.y2);
}

Vector3 MathFunctions::normal(Triangle3D tr) {
	int dx = (tr.y1 - tr.y0) * (tr.z2 - tr.z0) - (tr.z1 - tr.z0) * (tr.y2 - tr.y0);
	int dy = - (tr.x1 - tr.x0) * (tr.z2 - tr.z0) + (tr.z1 - tr.z0) * (tr.x2 - tr.x0);
	int dz = (tr.x1 - tr.x0) * (tr.y2 - tr.y0) - (tr.y1 - tr.y0) * (tr.x2 - tr.x0);
	return Vector3(dx, dy, dz);
}

Vector3f MathFunctions::normalize(Vector3 vec) {
	float len = length(vec);
	return Vector3f(vec.dx / len, vec.dy / len, vec.dz / len);
}

float MathFunctions::length(Vector3 vec) {
	return sqrt(vec.dx * vec.dx + vec.dy * vec.dy + vec.dz * vec.dz);
}

int MathFunctions::getZIntersection(const int X, const int Y, Triangle3D tr) {
	Vector3 n = normal(tr);
	if (n.dz == 0) {
		BoundingRect3D rect = boundingRectFromTriangle(tr);
		return rect.maxZ;
	}

	return tr.z0 - (n.dx * (X - tr.x0) + n.dy * (Y - tr.y0)) / n.dz;
}