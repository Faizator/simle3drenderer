#pragma once
#include <Windows.h>
#include "Log.h"
#include "Triangle2D.h"
using namespace Log;

namespace DrawingFunctions {
	class IPixelSetter {
	public:
		virtual void setPixel(int x, int y, COLORREF color) = 0;
	};

	void drawLine(IPixelSetter* pixelSetter, int x1, int y1, int x2, int y2, COLORREF color);

	void drawTriangle(IPixelSetter* pixelSetter, Triangle2D triangle, COLORREF color);
}