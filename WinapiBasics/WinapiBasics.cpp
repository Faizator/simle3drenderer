#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
#include <stdio.h>
#include "WInfo.h"
#include "MGame.h"
#include "Log.h"
using namespace Gdiplus;
using namespace Log;
#pragma comment (lib, "gdiplus.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HWND startupWindow(HINSTANCE hInstance, ULONG_PTR* ptr_gdiplusToken, INT iCmdShow);
void shutdownWindow(ULONG_PTR ptr_gdiplusToken);
void loop(HWND hWnd, MSG* ptr_msg);

Image* image;
MGame GAME = MGame();

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, PSTR, INT iCmdShow) {
	MSG msg;
	HWND hWnd;
	ULONG_PTR gdiplusToken;

	hWnd = startupWindow(hInstance, &gdiplusToken, iCmdShow);
	loop(hWnd, &msg);
	shutdownWindow(gdiplusToken);

	return msg.wParam;
}  // WinMain

void shutdownWindow(ULONG_PTR ptr_gdiplusToken) {
	GdiplusShutdown(ptr_gdiplusToken);
}

HWND startupWindow(HINSTANCE hInstance, ULONG_PTR* ptr_gdiplusToken, INT iCmdShow) {
	HWND                hWnd;
	WNDCLASS            wndClass;
	GdiplusStartupInput gdiplusStartupInput;

	// Initialize GDI+.
	GdiplusStartup(ptr_gdiplusToken, &gdiplusStartupInput, NULL);
	image = Image::FromFile(L"man.jpg");
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = NULL;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = TEXT("Simple 3D Graphics Engine");

	RegisterClass(&wndClass);

	hWnd = CreateWindow(
		TEXT("Simple 3D Graphics Engine"),   // window class name
		TEXT("Simple 3D Graphics Engine"),  // window caption
		WS_OVERLAPPEDWINDOW,      // window style
		CW_USEDEFAULT,            // initial x position
		CW_USEDEFAULT,            // initial y position
		600,            // initial x size
		600,            // initial y size
		NULL,                     // parent window handle
		NULL,                     // window menu handle
		NULL,                // program instance handle
		NULL);                    // creation parameters

	ShowWindow(hWnd, iCmdShow);
	UpdateWindow(hWnd);
	return hWnd;
}

void loop(HWND hWnd, MSG* ptr_msg) {
	GAME.isRunning = true;
	int i = 0;
	DWORD now = GetTickCount();
	DWORD then = now;
	const DWORD FRAME_RATE = 100; // 1000 ms / 60 fps = 17
	//const DWORD FRAME_RATE = 1000; // 1000 ms / 1 fps = 1000
	while (GAME.isRunning) {
		now = GetTickCount();
		const long DIFF = now - then;
		then = now;
		const long FRAME_DIFF = FRAME_RATE - DIFF;
		if (GAME.update()) {
			InvalidateRect(hWnd, NULL, FALSE);
		}
		if (FRAME_DIFF > 0) {
			Sleep(FRAME_DIFF);
		}

		i++;
		if (PeekMessage(ptr_msg, NULL, 0, 0, PM_NOREMOVE)) {
			if (GetMessage(ptr_msg, NULL, 0, 0)) {
				TranslateMessage(ptr_msg);
				DispatchMessage(ptr_msg);
			}
			else {
				GAME.isRunning = false;
			}
		}
	}
}

LRESULT WINAPI WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	HDC          hdc;
	PAINTSTRUCT  ps;
	
	//debug("WND %d", message);
	switch(message) {
		case WM_ENTERSIZEMOVE:
			GAME.paused = true;
			return 0;
		case WM_EXITSIZEMOVE:
			GAME.paused = false;
			return 0;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			if (!GAME.paused) {
				GAME.onPaint(hWnd, &ps);
			}
			EndPaint(hWnd, &ps);
			return 0;
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		case WM_LBUTTONDOWN:
			debug("XXX\n");
			BringWindowToTop(hWnd);
			GAME.lButtonPressed = true;
			return 0;
		case WM_LBUTTONUP:
			GAME.lButtonPressed = false;
			return 0;
		case WM_ERASEBKGND:
			return (LRESULT)1; // Say we handled it.
		
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
} // WndProc