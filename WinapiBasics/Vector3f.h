#pragma once
class Vector3f {
public:
	float dx, dy, dz;

	Vector3f(float dx, float dy, float dz) {
		this->dx = dx;
		this->dy = dy;
		this->dz = dz;
	}
};