#pragma once
class Vector3 {
public:
	int dx, dy, dz;

	Vector3(int dx, int dy, int dz) {
		this->dx = dx;
		this->dy = dy;
		this->dz = dz;
	}
};