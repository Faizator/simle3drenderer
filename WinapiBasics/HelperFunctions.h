#pragma once
#include "Vector3f.h"
#include "MathFunctions.h"
#include <math.h>
#include <windows.h>

namespace HelperFunctions {
	void swap(int* a, int* b);
	int mod(int a);
	double mod(double a);

	Vector3f rgbToHsv(Vector3f RGB);
	Vector3f hsvToRgb(Vector3f HSV);

	COLORREF rgbToColorref(Vector3f RGB);
	Vector3f colorrefToRgb(COLORREF color);
}