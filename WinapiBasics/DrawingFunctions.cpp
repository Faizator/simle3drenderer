#include <Windows.h>
#include "Log.h"
#include "HelperFunctions.h"
#include "DrawingFunctions.h"
#include "Triangle2D.h"
using namespace Log;
using namespace HelperFunctions;

namespace DrawingFunctions {
	/* 
		Draw line, using Bresenham's line algorithm 
	*/
	void drawLine(IPixelSetter* pixelSetter, int x1, int y1, int x2, int y2, COLORREF color) {
		int dx = x2 - x1;
		int dy = y2 - y1;

		boolean swappedXY = false;
		if (mod(dy) > mod(dx)) {
			swap(&x1, &y1);
			swap(&x2, &y2);
			swap(&dx, &dy);
			swappedXY = true;
		}

		if (dx < 0) {
			swap(&x1, &x2);
			swap(&y1, &y2);
		}

		double error = 0;
		double coeff = 1.0 * (dy) / (dx);
		debug("COEFF %f", coeff);

		int y = y1;
		for (int x = x1; x < x2; x++) {
			error += coeff;
			if (error > 1) {
				++y;
				--error;
			} else if (error < -1) {
				--y;
				++error;
			}
			if (swappedXY) {
				pixelSetter->setPixel(y, x, color);
			} else {
				pixelSetter->setPixel(x, y, color);
			}
		}
	}


	void drawTriangle(IPixelSetter* pixelSetter, Triangle2D triangle, COLORREF color) {
		drawLine(pixelSetter, triangle.x0, triangle.y0, triangle.x1, triangle.y1, color);
		drawLine(pixelSetter, triangle.x1, triangle.y1, triangle.x2, triangle.y2, color);
		drawLine(pixelSetter, triangle.x0, triangle.y0, triangle.x2, triangle.y2, color);
	}
}