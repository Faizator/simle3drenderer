#include "Log.h"

void Log::debug(const char * msg) {
	debug(msg, 0);
}

void Log::debug(const char * msg, double num) {
	char out[256];
	sprintf_s(out, msg, num);
	OutputDebugStringA(out);
	OutputDebugStringA("\n");
}

void Log::debug(const char * msg, int num) {
	char out[256];
	sprintf_s(out, msg, num);
	OutputDebugStringA(out);
	OutputDebugStringA("\n");
}
