#pragma once
class BoundingRect3D {
public:
	int minX, maxX, minY, maxY, minZ, maxZ, leftZ, rightZ;

	BoundingRect3D(int minX, int maxX, int minY, int maxY, int leftZ, int rightZ) {
		this->minX = minX;
		this->minY = minY;
		this->minZ = leftZ < rightZ ? leftZ : rightZ;

		this->maxX = maxX;
		this->maxY = maxY;
		this->maxZ = leftZ < rightZ ? rightZ : leftZ;

		this->leftZ = leftZ;
		this->rightZ = rightZ;
	}
};