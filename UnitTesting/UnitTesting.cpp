#define CATCH_CONFIG_MAIN

#include "catch.h"
#include "HelperFunctions.h"
#include "MathFunctions.h"
#include "Vector3.h"
#include "Vector3f.h"

unsigned int Factorial(unsigned int number) {
	return number <= 1 ? number : Factorial(number - 1)*number;
}

TEST_CASE("Helper functions", "[helper_functions]") {
	
	SECTION("swap") {
		int a = 5;
		int b = 15;
		HelperFunctions::swap(&a, &b);
		REQUIRE(a == 15);
		REQUIRE(b == 5);
		HelperFunctions::swap(&a, &b);
		REQUIRE(a == 5);
		REQUIRE(b == 15);
	}

	SECTION("mod") {
		int a = 5;
		int b = -5;
		a = HelperFunctions::mod(a);
		b = HelperFunctions::mod(b);
		REQUIRE(a == 5);
		REQUIRE(b == 5);
	}

	SECTION("rgbToHsv") {
		Vector3f RGB_WHITE = Vector3f(255, 255, 255);
		Vector3f RGB_BLACK = Vector3f(0, 0, 0);
		Vector3f RGB_LIME = Vector3f(0, 255, 0);
		Vector3f RGB_GREEN = Vector3f(0, 128, 0);
		Vector3f RGB_CYAN = Vector3f(0, 255, 255);
		Vector3f RGB_TEAL = Vector3f(0, 128, 128);
		
		Vector3f HSV = HelperFunctions::rgbToHsv(RGB_WHITE);
		REQUIRE(HSV.dx == 0);
		REQUIRE(HSV.dy == 0);
		REQUIRE(HSV.dz == 1);

		HSV = HelperFunctions::rgbToHsv(RGB_BLACK);
		REQUIRE(HSV.dx == 0);
		REQUIRE(HSV.dy == 0);
		REQUIRE(HSV.dz == 0);

		HSV = HelperFunctions::rgbToHsv(RGB_LIME);
		REQUIRE(HSV.dx == 120);
		REQUIRE(HSV.dy == 1);
		REQUIRE(HSV.dz == 1);

		HSV = HelperFunctions::rgbToHsv(RGB_GREEN);
		REQUIRE(HSV.dx == 120);
		REQUIRE(HSV.dy == 1);
		REQUIRE(abs(HSV.dz - 0.5) < 0.1);

		HSV = HelperFunctions::rgbToHsv(RGB_CYAN);
		REQUIRE(HSV.dx == 180);
		REQUIRE(HSV.dy == 1);
		REQUIRE(HSV.dz == 1);

		HSV = HelperFunctions::rgbToHsv(RGB_TEAL);
		REQUIRE(HSV.dx == 180);
		REQUIRE(HSV.dy == 1);
		REQUIRE(abs(HSV.dz - 0.5) < 0.1);
	}

	SECTION("hsvToRgb") {
		Vector3f HSV_WHITE = Vector3f(0, 0, 1);
		Vector3f HSV_BLACK = Vector3f(0, 0, 0);
		Vector3f HSV_LIME = Vector3f(120, 1, 1);
		Vector3f HSV_GREEN = Vector3f(120, 1, 0.5);
		Vector3f HSV_CYAN = Vector3f(180, 1, 1);
		Vector3f HSV_TEAL = Vector3f(180, 1, 0.5);

		Vector3f RGB = HelperFunctions::hsvToRgb(HSV_WHITE);
		REQUIRE(RGB.dx == 255);
		REQUIRE(RGB.dy == 255);
		REQUIRE(RGB.dz == 255);

		RGB = HelperFunctions::hsvToRgb(HSV_BLACK);
		REQUIRE(RGB.dx == 0);
		REQUIRE(RGB.dy == 0);
		REQUIRE(RGB.dz == 0);

		RGB = HelperFunctions::hsvToRgb(HSV_LIME);
		REQUIRE(RGB.dx == 0);
		REQUIRE(RGB.dy == 255);
		REQUIRE(RGB.dz == 0);

		RGB = HelperFunctions::hsvToRgb(HSV_GREEN);
		REQUIRE(RGB.dx == 0);
		REQUIRE(abs(RGB.dy - 128) < 1);
		REQUIRE(RGB.dz == 0);

		RGB = HelperFunctions::hsvToRgb(HSV_CYAN);
		REQUIRE(RGB.dx == 0);
		REQUIRE(RGB.dy == 255);
		REQUIRE(RGB.dz == 255);

		RGB = HelperFunctions::hsvToRgb(HSV_TEAL);
		REQUIRE(RGB.dx == 0);
		REQUIRE(abs(RGB.dy - 128) < 1);
		REQUIRE(abs(RGB.dz - 128) < 1);
	}

	SECTION("rgbToColorref") {
		Vector3f RGB_WHITE = Vector3f(255, 255, 255);
		Vector3f RGB_BLACK = Vector3f(0, 0, 0);
		Vector3f RGB_LIME = Vector3f(0, 255, 0);
		Vector3f RGB_GREEN = Vector3f(0, 128, 0);
		Vector3f RGB_CYAN = Vector3f(0, 255, 255);
		Vector3f RGB_TEAL = Vector3f(0, 128, 128);

		COLORREF color = HelperFunctions::rgbToColorref(RGB_WHITE);
		REQUIRE(color == 0xFFFFFF);

		color = HelperFunctions::rgbToColorref(RGB_BLACK);
		REQUIRE(color == 0);

		color = HelperFunctions::rgbToColorref(RGB_LIME);
		REQUIRE(color == 0x00FF00);

		color = HelperFunctions::rgbToColorref(RGB_GREEN);
		REQUIRE(color == 0x008000);

		color = HelperFunctions::rgbToColorref(RGB_CYAN);
		REQUIRE(color == 0xFFFF00);

		color = HelperFunctions::rgbToColorref(RGB_TEAL);
		REQUIRE(color == 0x808000);
	}

	SECTION("colorrefToRgb") {
		COLORREF COLOR_WHITE = 0xFFFFFF;
		COLORREF COLOR_BLACK = 0x000000;
		COLORREF COLOR_LIME = 0x00FF00;
		COLORREF COLOR_GREEN = 0x008000;
		COLORREF COLOR_CYAN = 0xFFFF00;
		COLORREF COLOR_TEAL = 0x808000;

		Vector3f rgb = HelperFunctions::colorrefToRgb(COLOR_WHITE);
		REQUIRE(rgb.dx == 255);
		REQUIRE(rgb.dy == 255);
		REQUIRE(rgb.dz == 255);

		rgb = HelperFunctions::colorrefToRgb(COLOR_BLACK);
		REQUIRE(rgb.dx == 0);
		REQUIRE(rgb.dy == 0);
		REQUIRE(rgb.dz == 0);

		rgb = HelperFunctions::colorrefToRgb(COLOR_LIME);
		REQUIRE(rgb.dx == 0);
		REQUIRE(rgb.dy == 255);
		REQUIRE(rgb.dz == 0);

		rgb = HelperFunctions::colorrefToRgb(COLOR_GREEN);
		REQUIRE(rgb.dx == 0);
		REQUIRE(rgb.dy == 128);
		REQUIRE(rgb.dz == 0);

		rgb = HelperFunctions::colorrefToRgb(COLOR_CYAN);
		REQUIRE(rgb.dx == 0);
		REQUIRE(rgb.dy == 255);
		REQUIRE(rgb.dz == 255);

		rgb = HelperFunctions::colorrefToRgb(COLOR_TEAL);
		REQUIRE(rgb.dx == 0);
		REQUIRE(rgb.dy == 128);
		REQUIRE(rgb.dz == 128);
	}
}

TEST_CASE("Math functions", "[math_functions]") {

	SECTION("gradToRad") {
		double rad = MathFunctions::gradToRad(180);
		double diff = HelperFunctions::mod(rad - PI);
		REQUIRE(diff < 0.1);

		rad = MathFunctions::gradToRad(360);
		diff = HelperFunctions::mod(rad - 2*PI);
		REQUIRE(diff < 0.1);

		rad = MathFunctions::gradToRad(720);
		diff = HelperFunctions::mod(rad - 4 * PI);
		REQUIRE(diff < 0.1);

		rad = MathFunctions::gradToRad(0);
		diff = HelperFunctions::mod(rad - 0);
		REQUIRE(diff < 0.1);

		rad = MathFunctions::gradToRad(90);
		diff = HelperFunctions::mod(rad - PI/2);
		REQUIRE(diff < 0.1);

		rad = MathFunctions::gradToRad(90);
		diff = HelperFunctions::mod(rad - PI / 2);
		REQUIRE(diff < 0.1);

		rad = MathFunctions::gradToRad(-90);
		diff = HelperFunctions::mod(rad - (- PI / 2));
		REQUIRE(diff < 0.1);
	}

	SECTION("minOf_maxOf") {
		int a = 5;
		int b = 2;
		int c = 12;

		REQUIRE(MathFunctions::minOf(a, b) == 2);
		REQUIRE(MathFunctions::maxOf(a, b) == 5);
		REQUIRE(MathFunctions::minOf(b, a) == 2);
		REQUIRE(MathFunctions::maxOf(b, a) == 5);

		REQUIRE(MathFunctions::minOf(b, c) == 2);
		REQUIRE(MathFunctions::maxOf(b, c) == 12);

		REQUIRE(MathFunctions::minOf(a, b, c) == 2);
		REQUIRE(MathFunctions::maxOf(a, b, c) == 12);
		REQUIRE(MathFunctions::minOf(c, a, b) == 2);
		REQUIRE(MathFunctions::maxOf(c, a, b) == 12);
	}

	SECTION("pseudoScalar") {
		Vector2 a1(3, 5);
		Vector2 b1(-4, 2);

		Vector2 a2(25, 30);
		Vector2 b2(13, 7);

		REQUIRE(MathFunctions::pseudoScalar(a1, b1) == 26);
		REQUIRE(MathFunctions::pseudoScalar(a2, b2) == -215);
	}

	SECTION("to2D") {
		Triangle3D tri3D = Triangle3D(
			100, 50, 50,
			400, 180, 100,
			100, 200, 50
		);
		Triangle2D tri2D = MathFunctions::to2D(tri3D);

		REQUIRE(tri2D.x0 == tri3D.x0);
		REQUIRE(tri2D.y0 == tri3D.y0);

		REQUIRE(tri2D.x1 == tri3D.x1);
		REQUIRE(tri2D.y1 == tri3D.y1);

		REQUIRE(tri2D.x2 == tri3D.x2);
		REQUIRE(tri2D.y2 == tri3D.y2);
	}

	SECTION("boundingRectFromTriangle") {
		Triangle3D tri3D = Triangle3D(
			100, 50, 50,
			400, 180, 100,
			100, 200, 50
		);

		Triangle2D tri2D = MathFunctions::to2D(tri3D);

		BoundingRect3D br3D = MathFunctions::boundingRectFromTriangle(tri3D);
		REQUIRE(br3D.minX == 100);
		REQUIRE(br3D.maxX == 400);

		REQUIRE(br3D.minY == 50);
		REQUIRE(br3D.maxY == 200);

		REQUIRE(br3D.minZ == 50);
		REQUIRE(br3D.maxZ == 100);

		REQUIRE(br3D.leftZ == 50);
		REQUIRE(br3D.rightZ == 100);

		BoundingRect2D br2D = MathFunctions::boundingRectFromTriangle(tri2D);
		REQUIRE(br2D.minX == 100);
		REQUIRE(br2D.maxX == 400);

		REQUIRE(br2D.minY == 50);
		REQUIRE(br2D.maxY == 200);
	}

	SECTION("pointInTriangle") {
		Triangle2D triangle = Triangle2D(
			0, 0, 
			200, 0,
			100, 100
		);
		
		Point2 pointsIn[] = {
			Point2(100, 50),
			Point2(110, 40)
		};

		Point2 pointsOut[] = {
			Point2(10, 150),
			Point2(190, 140)
		};

		REQUIRE(MathFunctions::pointInTriangle(triangle, pointsIn[0]));
		REQUIRE(MathFunctions::pointInTriangle(triangle, pointsIn[1]));
	
		REQUIRE_FALSE(MathFunctions::pointInTriangle(triangle, pointsOut[0]));
		REQUIRE_FALSE(MathFunctions::pointInTriangle(triangle, pointsOut[1]));
	}

	SECTION("normal") {
		Triangle3D tr = Triangle3D(
			0, 0, 0,
			200, 0, 100,
			100, 100, 50
		);

		Vector3 normal = MathFunctions::normal(tr);
		REQUIRE(normal.dx == -10000);
		REQUIRE(normal.dy == 0);
		REQUIRE(normal.dz == 20000);
	}

	SECTION("getZIntersection") {
		Triangle3D tr = Triangle3D(
			0, 0, 50,
			200, 0, 50,
			100, 100, 50
		);

		Triangle3D tr2 = Triangle3D(
			0, 0, 0,
			200, 0, 100,
			100, 100, 50
		);

		const int X = 50;
		const int Y = 10;

		int z1 = MathFunctions::getZIntersection(X, Y, tr);
		int z2 = MathFunctions::getZIntersection(X, Y, tr2);
		
		REQUIRE(z1 == 50);
		REQUIRE(z2 == 25);
	}

	SECTION("length") {
		Vector3 vec1 = Vector3(3, 4, 0);
		Vector3 vec2 = Vector3(1, 0, 1);
		Vector3 vec3 = Vector3(2, 2, 2);

		REQUIRE(MathFunctions::length(vec1) == 5);
		REQUIRE(MathFunctions::length(vec2) - 1.41 < 0.1);
		REQUIRE(MathFunctions::length(vec3) - 3.46 < 0.1);
	}

	SECTION("normalize") {
		Vector3 vec1 = Vector3(3, 4, 0);
		Vector3 vec2 = Vector3(1, 0, 1);
		Vector3 vec3 = Vector3(2, 2, 2);

		Vector3f vec1N = MathFunctions::normalize(vec1);
		REQUIRE(vec1N.dx - 3.0f / 5 < 0.1);
		REQUIRE(vec1N.dx - 3.0f / 5 >= 0);
		REQUIRE(vec1N.dy - 4.0f / 5 < 0.1);
		REQUIRE(vec1N.dy - 4.0f / 5 >= 0);
		REQUIRE(vec1N.dz == 0);
	}
}
