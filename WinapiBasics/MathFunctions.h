#pragma once
#include "Triangle2D.h"
#include "Triangle3D.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector3f.h"
#include "Point2.h"
#include "BoundingRect2D.h"
#include "BoundingRect3D.h"
#define PI 3.14159265

namespace MathFunctions {
	double gradToRad(int grad);
	int minOf(int a, int b);
	int minOf(int a, int b, int c);
	int maxOf(int a, int b);
	int maxOf(int a, int b, int c);
	int pseudoScalar(Vector2 a, Vector2 b);
	BoundingRect2D boundingRectFromTriangle(Triangle2D triangle);
	BoundingRect3D boundingRectFromTriangle(Triangle3D triangle);
	bool pointInTriangle(Triangle2D triangle, Point2 point);
	Triangle2D to2D(Triangle3D triangle3D);
	Vector3 normal(Triangle3D triangle);
	Vector3f normalize(Vector3 vec);
	float length(Vector3 vec);
	int getZIntersection(const int X, const int Y, Triangle3D tri);
}