#include "WInfo.h"


void WInfo::update(HWND hwnd) {
	GetClientRect(hwnd, &rect);
};

WInfo::WInfo() {};

WInfo::WInfo(HWND hwnd) {
	update(hwnd);
};

WInfo::WInfo(RECT& rc) {
	this->rect = rc;
};

int WInfo::getWidth() {
	return rect.right - rect.left;
};

int WInfo::getHeight() {
	return rect.bottom - rect.top;
};

int WInfo::getLeft() {
	return rect.left;
};

int WInfo::getRight() {
	return rect.right;
};

int WInfo::getTop() {
	return rect.top;
};

int WInfo::getBottom() {
	return rect.bottom;
};
