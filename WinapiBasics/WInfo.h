#pragma once
#include <windows.h>

class WInfo {
public:
	RECT rect;
	void update(HWND hwnd);
	WInfo();
	WInfo(HWND hwnd);
	WInfo(RECT& rect);
	int getWidth();
	int getHeight();
	int getLeft();
	int getRight();
	int getTop();
	int getBottom();
};