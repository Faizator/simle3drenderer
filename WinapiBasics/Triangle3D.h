#pragma once
class Triangle3D {
public:
	int x0, y0, z0, x1, y1, z1, x2, y2, z2;

	Triangle3D(int x0, int y0, int z0, int x1, int y1, int z1, int x2, int y2, int z2) {
		this->x0 = x0;
		this->y0 = y0;
		this->z0 = z0;

		this->x1 = x1;
		this->y1 = y1;
		this->z1 = z1;

		this->x2 = x2;
		this->y2 = y2;
		this->z2 = z2;
	}
};