#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
#include <stdio.h>
#include "WInfo.h"
#include "MGame.h"
#include "Log.h"
#include "DrawingFunctions.h"
#include "MathFunctions.h"
#include "HelperFunctions.h"
#include <math.h>
#include "Model.h"
#include "Geometry.h"
using namespace Gdiplus;
using namespace MathFunctions;

extern Image* image;

Model* model = NULL;

/*
	Put all your drawing routine here.
	DO NOT CALL IT DIRECTLY!
*/
void MGame::innerPaint(HDC hdc, RECT& rc) {
	if (model == NULL) {
		model = new Model("african_head.obj");
	}
	WInfo wInfo(rc);
	Graphics graphics(hdc);
	Pen      pen(Color(255, 100, 100, 255));
	SolidBrush mySolidBrush(Color(0, 0, 0, 255));
	HdcPixelSetter* ptr_pixelSetter = &HdcPixelSetter(hdc);
	const COLORREF RED_COLOR = 0x0000FF;
	const COLORREF BLUE_COLOR = 0xFF0000;
	const COLORREF GREEN_COLOR = 0xFFFFFF;

	// Draw background
	graphics.FillRectangle(&mySolidBrush, 0, 0, wInfo.getWidth(), wInfo.getHeight());

	//// Draw image
	//graphics.DrawImage(image, 0, 400, 200, 200);
	//// Draw wide arrow
	//graphics.DrawLine(&pen, wInfo.getWidth() - 10, 100, wInfo.getWidth() - 50, 50);
	//graphics.DrawLine(&pen, 10, 100, wInfo.getWidth() - 20, 100);
	//graphics.DrawLine(&pen, wInfo.getWidth() - 20, 100, wInfo.getWidth() - 50, 150);
	//// Draw red line, using custom drawing function
	//HdcPixelSetter* ptr_pixelSetter = &HdcPixelSetter(hdc);
	//const int radius = 100;
	//const int centerX = 100;
	//const int centerY = 300;
	//
	//DrawingFunctions::drawLine(ptr_pixelSetter, 
	//	centerX, 
	//	centerY, 
	//	centerX + (int) (radius * cos(gradToRad(grad))), 
	//	centerY + (int) (radius * sin(gradToRad(grad))), 
	//	color);
	// Draw blue line, using library gdi drawing function
	//graphics.DrawLine(&pen, this->xPos, 200, this->xPos, 300);

	//Triangle3D tri3D_1 = Triangle3D(
	//	100, 50, 50,
	//	400, 180, 100,
	//	100, 200, 50
	//);
	static int G = 0;
	G++;
	if (G > 200) {
		G = 0;
	}
	//Triangle3D tri3D_2 = Triangle3D(
	//	300, 100, 200,
	//	300, 200, 200 - G,
	//	150, 150, 1
	//);

	Triangle3D tri3D_1 = Triangle3D(
		100, 100, 100,
		200, 100, 100,
		200, 200, 100
	);

	Triangle3D tri3D_2 = Triangle3D(
		200, 200, 100,
		200, 100, 100,
		300, 150, 200 - G
	);

	Triangle3D triangles[2] = { tri3D_1, tri3D_2 };
	//COLORREF colors[2] = { RED_COLOR, BLUE_COLOR };
	COLORREF colors[2] = { GREEN_COLOR, GREEN_COLOR };
	
	int zbuffer[501][501] = {};

	//TGAImage image(width, height, TGAImage::RGB);
	//for (int i = 0; i<model->nfaces(); i++) {
	//	std::vector<int> face = model->face(i);
	//	for (int j = 0; j<3; j++) {
	//		Vec3f v0 = model->vert(face[j]);
	//		Vec3f v1 = model->vert(face[(j + 1) % 3]);
	//		int x0 = (v0.x + 1.)*width / 2.;
	//		int y0 = (v0.y + 1.)*height / 2.;
	//		int x1 = (v1.x + 1.)*width / 2.;
	//		int y1 = (v1.y + 1.)*height / 2.;
	//		line(x0, y0, x1, y1, image, white);
	//	}
	//}
	//for (int i = 0; i < 2; i++) {

	const int COEFF = 200;
	const float OFFSET = 1.5 * COEFF;
	for (int i = 0; i<model->nfaces(); i++) {
		std::vector<int> face = model->face(i);
		Triangle3D tri3D = Triangle3D(
			OFFSET + COEFF * model->vert(face[0]).x,
			OFFSET + COEFF * model->vert(face[0]).y,
			OFFSET + COEFF * model->vert(face[0]).z,
			
			OFFSET + COEFF * model->vert(face[1]).x,
			OFFSET + COEFF * model->vert(face[1]).y,
			OFFSET + COEFF * model->vert(face[1]).z,
			
			OFFSET + COEFF * model->vert(face[2]).x,
			OFFSET + COEFF * model->vert(face[2]).y,
			OFFSET + COEFF * model->vert(face[2]).z
		);
		// triangles[i];
		Triangle2D tri2D = to2D(tri3D);
		COLORREF color = GREEN_COLOR;// colors[i];
		BoundingRect2D br = boundingRectFromTriangle(tri2D);

		for (int x = br.minX; x < br.maxX; x++) {
			for (int y = br.minY; y < br.maxY; y++) {
				Point2 point = Point2(x, y);
				if (pointInTriangle(tri2D, point)) {
					int z = getZIntersection(x, y, tri3D);
					int zbufferValue = zbuffer[x][y];
					if (zbufferValue != 0 && zbufferValue > z) {
						continue;
					}
					zbuffer[x][y] = z;

					Vector3f n = normalize(normal(tri3D));
					Vector3f rgb = HelperFunctions::colorrefToRgb(color);
					Vector3f hsv = HelperFunctions::rgbToHsv(rgb);
					float intensity = hsv.dz * n.dz;
					rgb = HelperFunctions::hsvToRgb(Vector3f(hsv.dx, hsv.dy, intensity));
					COLORREF endColor = HelperFunctions::rgbToColorref(rgb);

					point.y = OFFSET - (point.y - OFFSET);
					ptr_pixelSetter->setPixel(point.x, point.y, endColor);
				}
			}
		}

		if (this->lButtonPressed) {
			Triangle2D tri2DCopy = tri2D;
			tri2DCopy.y0 = OFFSET - (tri2DCopy.y0 - OFFSET);
			tri2DCopy.y1 = OFFSET - (tri2DCopy.y1 - OFFSET);
			tri2DCopy.y2 = OFFSET - (tri2DCopy.y2 - OFFSET);
			DrawingFunctions::drawTriangle(ptr_pixelSetter, tri2DCopy, color);
		}
	}
} // innerPaint

bool MGame::update() {
	if (this->paused) {
		return false;
	}
	if (this->lButtonPressed) {
		this->xPos += 1;
		this->grad += 1;
		return true;
	}
	return false;
}

/*
	Double buffering painting handler.
	Calls innerPaint(...) internally.
	Try not override it's painting behaviour, instead, put your drawing routine in innerPaint(...) function.
	DO NOT CALL IT DIRECTLY!.
*/
void MGame::onPaint(HWND hWnd, LPPAINTSTRUCT lpPS) {
	RECT rc;
	HDC hdcMem;
	HBITMAP hbmMem, hbmOld;
	HBRUSH hbrBkGnd;

	GetClientRect(hWnd, &rc);
	hdcMem = CreateCompatibleDC(lpPS->hdc);
	hbmMem = CreateCompatibleBitmap(lpPS->hdc,
		rc.right - rc.left,
		rc.bottom - rc.top);
	hbmOld = (HBITMAP)SelectObject(hdcMem, hbmMem);

	innerPaint(hdcMem, rc);

	SetBkMode(hdcMem, TRANSPARENT);
	BitBlt(lpPS->hdc,
		0, 0,
		rc.right - rc.left, rc.bottom - rc.top,
		hdcMem,
		0, 0,
		SRCCOPY);

	SelectObject(hdcMem, hbmOld);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);
} // onPaint