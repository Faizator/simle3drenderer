#pragma once
class Triangle2D {
public:
	int x0, y0, x1, y1, x2, y2;

	Triangle2D(int x0, int y0, int x1, int y1, int x2, int y2) {
		this->x0 = x0;
		this->x1 = x1;
		this->x2 = x2;
		this->y0 = y0;
		this->y1 = y1;
		this->y2 = y2;
	}
};