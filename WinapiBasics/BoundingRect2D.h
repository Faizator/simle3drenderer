#pragma once
class BoundingRect2D {
public:
	int minX, maxX, minY, maxY;

	BoundingRect2D(int minX, int maxX, int minY, int maxY) {
		this->minX = minX;
		this->minY = minY;

		this->maxX = maxX;
		this->maxY = maxY;
	}
};